from bson import ObjectId

from ..connections.database import Database
from ..utils.common import JSON_values_arestring, JSON_fields_arevalid


def get_user(user_id):
    try:
        object_id = ObjectId(user_id)
        user = Database.get_collection_user.find_one(
            {'_id': object_id}, {'_id': 0, 'password': 0})
        return user
    except:
        return None


def user_files_arevalid(files):
    '''
    Check number of files, mimetype
    and file size(by using tell and seek to calculate size distance 
    from start to end)
    Added naive way of constant FILE_SIZE_THRESHOLD for checking size
    '''
    # Max file size = 10 Megabytes
    FILE_SIZE_TRESHOLD = 10000000
    if len(files) > 1:
        return {'general': 'Maximum file is 1'}
    for key in files:
        request_file = files[key]
        # Checks if file has correct mimetype or not
        if request_file.mimetype.split('/')[0] != 'image':
            return {'general': 'File {} should be an image'
                               .format(request_file.filename)}
        # Checks if file has correct size or not
        pos = request_file.tell()
        request_file.seek(0, 2)  # seek to end
        size = request_file.tell()
        request_file.seek(pos)  # back to original position
        if(size > FILE_SIZE_TRESHOLD):
            return {'general': 'File {} is too large'
                               .format(request_file.filename)}
    return None


def user_update_fields_has_error(data):
    try:
        # Check data validity, frontend error
        if not data:
            return {'general': 'Fields must be JSON'}
        if not user_fields_arevalid(data):
            return {'general': 'Fields does not match'}
        return False
    except:
        return {'general': 'Internal Error'}


def user_fields_arevalid(data):
    array = ['description', 'location']
    return JSON_values_arestring(data) and JSON_fields_arevalid(data, array)
