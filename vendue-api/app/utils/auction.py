from bson import ObjectId
from bson.json_util import dumps
from time import time

from ..connections.database import Database
from ..utils.common import JSON_values_arestring, JSON_fields_arevalid

# Max file size = 10 Megabytes
FILE_SIZE_TRESHOLD = 10000000

def auction_create_fields_has_error(data):
    try:
        # Check data validity, frontend error
        if not data:
            return {'general': 'Fields must be JSON'}
        if not auction_fields_arevalid(data):
            return {'general': 'Fields does not match'}
        # Quick time capture
        created_date = int(time())
        # Check individual fields
        if len(data['name']) == 0:
            return {'name': 'Should not be empty'}
        if len(data['initialBid']) == 0:
            return {'initialBid': 'Should not be empty'}
        if not data['initialBid'].isdigit():
            return {'initialBid': 'Should only contain digit'}
        data['initialBid'] = int(data['initialBid'])
        if len(data['condition']) == 0:
            return {'condition': 'Should not be empty'}
        if len(data['description']) == 0:
            return {'description': 'Should not be empty'}
        if not data['expiredDate'].isdigit():
            return {'general': 'does not receive unix timestamp'}
        # if (int(data['expiredDate'])-int(time())) < 3600:
        #     return {'general': 'Should give at least 1 hour of expiration date'}
        

        return False
    except:
        return {'general': 'Internal Error'}


def auction_fields_arevalid(data):
    array = ['name', 'initialBid', 'condition', 'description', 'expiredDate']
    return JSON_values_arestring(data) and JSON_fields_arevalid(data, array)


def files_arevalid(files):
    '''
    Check number of files, mimetype
    and file size(by using tell and seek to calculate size distance 
    from start to end)
    '''
    if len(files) > 4 or len(files) == 0:
        return {'general': 'Should have 1-4 files uploaded'}
    for key in files:
        request_file = files[key]
        # Checks if file has correct mimetype or not
        if request_file.mimetype.split('/')[0] != 'image':
            return {'general': 'File {} should be an image'
                               .format(request_file.filename)}
        # Checks if file has correct size or not
        pos = request_file.tell()
        request_file.seek(0, 2)  # seek to end
        size = request_file.tell()
        request_file.seek(pos)  # back to original position
        if(size > FILE_SIZE_TRESHOLD):
            return {'general': 'File {} is too large'
                               .format(request_file.filename)}

    return None


def delete_auction(auction_id):
    try:
        object_id = ObjectId(auction_id)
        auction = Database.get_collection_auction().delete_one({
            '_id': object_id})
        return auction
    except:
        return None


def update_auction_finish(auction_id):
    try:
        object_id = ObjectId(auction_id)
        result = Database.get_collection_auction().update(
            {'_id': object_id}, {'$set': {'hasFinished': True}})
        return True
    except:
        print('Auction is not available')
        return False
