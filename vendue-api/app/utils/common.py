def JSON_values_arestring(data):
    for key,value in data.items():
        if not (isinstance(value,str)):
            return False
    else:
        return True

def JSON_fields_arevalid(data,array):
    # Check length
    if len(array) != len(data): return False
    # Check if all keys matches
    for element in array:
        if element not in data:
            return False
    return True

def JSON_fields_in_array(data,array):
    for element in array:
        if element not in data:
            return False
    return True