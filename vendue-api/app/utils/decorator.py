from flask import request, jsonify, make_response
from os import getenv
import jwt
from ..connections.database import Database
from functools import wraps
from bson import ObjectId


def token_required(func):
    '''
    Auth decorator
    using wraps, decorate functions that requires jwt checking
    returns the user data
    '''
    @wraps(func)
    def decorated(*args, **kwargs):
        try:
            token = None
            if 'Authorization' in request.headers:
                token = request.headers['Authorization']
            if not token:
                return make_response(jsonify({'error': {'token': 'missing'}}), 401, {'WWW-Authenticate': 'Basic realm="Login required!"'})

            data = jwt.decode(token, getenv('SECRET_KEY'))
            user_id = ObjectId(data['_id'])
            current_user = Database.get_collection_user().find_one({
                '_id': user_id}, {'password': 0})

            if not current_user:
                return make_response(jsonify({'error': {'token': 'invalid'}}), 401, {'WWW-Authenticate': 'Basic realm="Login required!"'})

            current_user['_id'] = str(current_user['_id'])
        except jwt.DecodeError:
            return make_response(jsonify({'error': {'token': 'cannot decode'}}), 401, {'WWW-Authenticate': 'Basic realm="Login required!"'})
        except jwt.ExpiredSignatureError:
            return make_response(jsonify({'error': {'token': 'expired'}}), 401, {'WWW-Authenticate': 'Basic realm="Login required!"'})
        except:
            return jsonify({'error': 'Internal Error'}), 500
        return func(current_user, *args, **kwargs)

    return decorated
