import re
from zxcvbn import zxcvbn

from .common import JSON_fields_arevalid, JSON_values_arestring

from ..connections.database import Database


def register_fields_has_error(data):
    # Check data validity, frontend error
    if not data:
        return {'general': 'Fields must be JSON'}
    if not register_fields_arevalid(data):
        return {'general': 'Fields does not match'}
    # Check username
    if len(data['username']) < 3 or len(data['username']) > 20:
        return {'username': 'length should be between 3 to 20'}
    # Check email
    if not email_isvalid(data['email']):
        return {'email': 'Must be a proper email'}
    # Check password
    if data['password'] != data['confirmPassword']:
        return {'confirmPassword': 'Should match password'}

    return False


def register_fields_arevalid(data):
    array = ['username', 'email', 'password', 'confirmPassword']
    return JSON_values_arestring(data) and JSON_fields_arevalid(data, array)


def fields_already_taken(data):
    is_taken = username_istaken(data)
    if is_taken:
        return is_taken
    is_taken = email_istaken(data)
    if is_taken:
        return is_taken
    return False


def username_istaken(data):
    if Database.get_collection_user().find_one({'username': data['username']}):
        return {'username': 'Already taken'}
    return False


def email_istaken(data):
    if Database.get_collection_user().find_one({'email': data['email']}):
        return {'email': 'Already taken'}
    return False


def email_isvalid(email):
    '''
    Regex manipulation to check email
    Email has to at least have `username`@`domain`
    Taken from Geeks for Geeks

    '''
    regex = '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'
    if not (re.search(regex, email)):
        return False
    else:
        return True


def review_password(password):
    METER = {0: 'terrible', 1: 'weak', 2: 'ok', 3: 'good', 4: 'great'}
    results = zxcvbn(password)
    if results['score'] < 2:
        if len(password) > 15:
            return {'error': {'password': 'length not be above 15'}}

        response = {'error': {'password': 'strength: {}, {}'.format(
            METER[results['score']
                  ], results['feedback']['suggestions'][0]
        )}}

        return response
    elif len(password) < 5:
        return {'error': {'password': 'length not be below 5'}}
    else:
        response = {'payload': 'strength: {}'.format(
            METER[results['score']]
        )}
        return response


def login_fields_has_error(data):
    # Check data validity, frontend error
    if not data:
        return {'general': 'Fields must be JSON'}
    if not login_fields_arevalid(data):
        return {'general': 'Fields does not match'}
    return False


def login_fields_arevalid(data):
    array = ['username', 'password']
    return JSON_values_arestring(data) and JSON_fields_arevalid(data, array)
