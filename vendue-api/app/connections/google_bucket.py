from google.cloud import storage
import google


class GoogleBucket:
    '''
    a singleton object that connects to Google Cloud Platform Bucket
    '''

    __instance = None

    @staticmethod
    def getConnectionInstance():
        if GoogleBucket.__instance == None:
            GoogleBucket()
        return GoogleBucket.__instance

    def init_connection():
        storage_client = storage.Client()
        bucket = storage_client.get_bucket('vendue')
        return bucket

    def list_blobs(bucketFolder='',bucketName='vendue'):
        '''List all files in Google Bucket.'''
        bucket = GoogleBucket.getConnectionInstance()
        files = bucket.list_blobs(prefix=bucketFolder)
        fileList = [file.name for file in files if '.' in file.name]
        return fileList

    def bucket_upload(data, filename):
        '''Upload files to Google Bucket.'''
        file_extension = data.filename.split('.')[-1]
        image_filename = '{}.{}'.format(filename, file_extension)
        blob = GoogleBucket.getConnectionInstance().blob(image_filename)

        blob.upload_from_file(data)
        blob.make_public()
        image_url = blob.public_url

        return image_url

    def delete_blob(fileName, bucketName='vendue', bucketFolder='/'):
        '''
        Delete file from Google Bucket
        TODO: need flexibility for function calls
        '''
        try:
            bucket = GoogleBucket.getConnectionInstance()
            response = bucket.delete_blob(bucketFolder + fileName)
            return f'{fileName} deleted from bucket. {response}'
        except google.api_core.exceptions.NotFound:
            print('was not there')
            return None

    def __init__(self):
        '''
        Create connection by giving JSON credentials
        '''
        if GoogleBucket.__instance != None:
            raise Exception(
                "attempt to instantiate another singleton Google Bucket")
        else:
            print('initializing Google Bucket...')
            GoogleBucket.__instance = GoogleBucket.init_connection()
