from flask import current_app
from flask_mail import Mail, Message


class FlaskMail:
    '''
    a singleton object that connects to Flask Mail
    '''
    __instance = None

    @staticmethod
    def getConnectionInstance():
        if FlaskMail.__instance == None:
            FlaskMail()
        return FlaskMail.__instance

    def init_connection():
        mail = Mail(current_app)
        return mail

    @staticmethod
    def send_confirmation_email(username, recipient, token):
        try:
            mail = FlaskMail.getConnectionInstance()
            msg = Message("Email confirmation",
                          sender="noreply.vendue@gmail.com",
                          recipients=[recipient])
            url = 'http://vendue.michaelsudirman.com/validate/e/{}'.format(
                token)
            msg.body = 'Hi {}!\n\n'.format(username) + \
                'Thank you for registering to vendue, Please use the ' \
                'confirmation link below to validate your account ' \
                '(valid until 1 day only):\n' + url
            mail.send(msg)
            return 'Mail sent!  '
        except Exception as e:
            print(e)
            return False

    @staticmethod
    def send_reset_token(username, recipient, token):
        try:
            mail = FlaskMail.getConnectionInstance()
            msg = Message("Reset Password",
                            sender="noreply.vendue@gmail.com",
                            recipients=[recipient])
            url = 'http://vendue.michaelsudirman.com/reset/t/{}'.format(
                token)
            msg.body= "Hi {}!\n\n".format(username) + \
                "Someone (hopefully you) asked us to reset your account's password." \
                "Please click the link to do so. " \
                "If you didn't ask for the password reset, please ignore \n"  \
                + url
            mail.send(msg)
            return 'Mail sent!'
        except Exception as e:
            print(e)
            return False

    def __init__(self):
        '''
        Create connection
        '''
        if FlaskMail.__instance != None:
            raise Exception(
                "attempt to instantiate another singleton Flask Mail")
        else:
            print('initializing Flask Mail...')
            FlaskMail.__instance = FlaskMail.init_connection()
