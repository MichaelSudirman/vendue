from pymongo import MongoClient
from os import getenv


class Database:
    '''
    a singleton object that connects to Google Cloud Platform Bucket
    '''

    __instance = None

    @staticmethod
    def getConnectionInstance():
        if Database.__instance == None:
            Database()
        return Database.__instance
    
    def init_connection():
        mongodb_id = getenv('MONGODB_ID')
        mongodb_password = getenv('MONGODB_PASSWORD')

        mongodb_uri = "mongodb://" + \
            mongodb_id + ":" + \
            mongodb_password + "@cluster0-shard-00-00-xxxaw.mongodb.net:27017,cluster0-shard-00-01-xxxaw.mongodb.net:27017,cluster0-shard-00-02-xxxaw.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority"

        mongodb_client = MongoClient(mongodb_uri,connect=False)
        vendue_database = mongodb_client.get_database('vendue')
        return vendue_database

    def get_collection_user():
        return Database.getConnectionInstance().user

    def get_collection_auction():
        return Database.getConnectionInstance().auction

    def get_collection_comment():
        return Database.getConnectionInstance().comment

    def get_collection_token():
        return Database.getConnectionInstance().token

    def get_collection_like():
        return Database.getConnectionInstance().like

    def __init__(self):
        '''
        Create connection by fetching credentials from environment
        '''
        if Database.__instance != None:
            raise Exception("attempt to instantiate another singleton Database")
        else:
            print('initializing Database...')
            Database.__instance = Database.init_connection()

