from os import getenv
from pika import URLParameters, BlockingConnection, BasicProperties
import pika
from threading import Thread
from uuid import uuid4
from ..utils.auction import update_auction_finish

class MessageQueue:
    '''
    a singleton object that connects to Rabbit Message Query Cloud Server
    '''

    __instance = None

    @staticmethod
    def getConnectionInstance():
        if MessageQueue.__instance == None:
            MessageQueue()
        return MessageQueue.__instance

    def init_connection():
        url = getenv('AMQP_URL')
        param = URLParameters(url)
        connection = BlockingConnection(param)

        return connection

    def init_listener_channel():
        ''' 
        Initiate Listener Thread
        Different Connection block must be made per thread
        because pika library is not thread-safe
        '''
        print("Initializing Rabbit Message Queue's Listener Thread...")
        listener_channel = MessageQueue.init_connection().channel()
        listener_channel.confirm_delivery()
        listener_channel.queue_declare(queue='listener', durable=True)

        # We need to bind this channel to an exchange, that will be used to transfer
        # messages from our delay queue.
        listener_channel.queue_bind(exchange='amq.direct', queue='listener')
        # Listener consume data
        listener_channel.basic_consume(
            queue='listener', on_message_callback=MessageQueue.call_finish_auction,
            auto_ack=True)
        listener_channel.start_consuming()

    @staticmethod
    def call_finish_auction(ch, method, properties, body):
        auction_id = body.decode("utf-8") 
        update_auction_finish(auction_id)

    @staticmethod
    def auction_timer(auction_id, time_delay):
        ''' 
        Initiate Auction Timer Queue
        Each message will be stored in a diffferent queue with expiration time
        When expired, publish to listener and end the auction
        '''
        try:
            # Create our delay channel.
            delay_channel = MessageQueue.getConnectionInstance().channel()
            delay_channel.confirm_delivery()
            
            message_id = str(uuid4())
            auction_delay = time_delay *1000
            # This is where we declare the delay, and routing for our delay channel.
            delay_channel.queue_declare(queue=message_id, durable=False,  arguments={
                'x-expires': auction_delay + 2000,
                # Delay until the message is transferred in milliseconds.
                'x-message-ttl': auction_delay,
                # Exchange used to transfer the message from A to B.
                'x-dead-letter-exchange': 'amq.direct',
                # Name of the queue we want the message transferred to.
                'x-dead-letter-routing-key': 'listener'
            })

            delay_channel.basic_publish(exchange='',
                                        routing_key=message_id,
                                        body=auction_id,
                                        properties=BasicProperties(delivery_mode=2))

            return True
        except pika.exceptions.ConnectionWrongStateError:
            # Pika is not thread safe, handle subprocess overlap
            MessageQueue.__instance = None
            MessageQueue()
            

    def __init__(self):
        if MessageQueue.__instance != None:
            raise Exception("attempt to instantiate another singleton Message Query")
        else:
            print('initializing Rabbit Message Queue...')
            MessageQueue.__instance = MessageQueue.init_connection()

            # Call another thread for listener channel
            thread = Thread(
                target=lambda: MessageQueue.init_listener_channel())
            thread.start()
