from setuptools import setup

'''
Create package using setuptools
Set the application name into app
Enables gunicorn to access the app folder
'''
setup(
    name='app',
    packages=['app'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
)