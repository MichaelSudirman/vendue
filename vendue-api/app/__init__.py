from flask import Flask
from os import path, getenv, environ
from flask_cors import CORS
from flask_bcrypt import Bcrypt
# Internal imports
from .api.default import default
from .api.auth import auth
from .api.user import user
from .api.auction import auction
from .api.test import test
# from flask_mail import Mail, Message
# initialize app
app = Flask(__name__)

# Initialize CORS
CORS(app, supports_credentials=True)
# CORS(app, resources={r"/*": {"origins": "localhost"}}, supports_credentials=True)

# Initialize Bcrypt
bcrypt = Bcrypt(app)

# Initialize secret key environment
app.config['SECRET_KEY'] = getenv('SECRET_KEY')

# Flask mail setup
app.config.update(
    DEBUG=True,
    # Cookie Settings
    # HOST='localhost',
    # SESSION_COOKIE_DOMAIN=False,
    # Email Settings
    MAIL_SERVER='smtp.gmail.com',
    MAIL_PORT=465,
    MAIL_USE_TTL=False,
    MAIL_USE_SSL=True,
    MAIL_USERNAME=getenv('MAIL_USERNAME'),
    MAIL_PASSWORD=getenv('MAIL_PASSWORD')
)

# Register blueprint routes
app.register_blueprint(default)
app.register_blueprint(auth)
app.register_blueprint(user, url_prefix='/user')
app.register_blueprint(auction, url_prefix='/auction')
app.register_blueprint(test, url_prefix='/test')
