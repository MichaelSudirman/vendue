from flask import Blueprint, request, jsonify, make_response
from bson import ObjectId
from bson.json_util import dumps
from json import loads
from uuid import uuid4

from ..utils.user import get_user, user_files_arevalid, user_update_fields_has_error
from ..utils.decorator import token_required
from ..connections.database import Database
from ..connections.google_bucket import GoogleBucket
from ..connections.flask_mail import FlaskMail

user = Blueprint('user', __name__)


@user.route('/', methods=['GET'])
def index():
    return jsonify({'payload': 'This is user route'})


@user.route('/<user_id>', methods=['GET'])
def user_read(user_id):
    user = get_user(user_id)

    if not user:
        return jsonify({'error': 'Could not find the user'}), 400
    else:
        return jsonify({'payload': {'user': user}})


@user.route('/me', methods=['GET'])
@token_required
def self_read(current_user):
    user = loads(dumps(current_user))
    return jsonify({'payload': {'user': user}})


@user.route('/update', methods=['PUT'])
@token_required
def user_update(current_user):
    '''
    Update profile
    1. Checks for field error
    2. Under constraints, will delete the image first if it's not the default image
    3. Update image
    Because of previous constraint, only 1 loop is triggered
    '''
    # Fetch input and check for credentials
    request_dict = request.values.to_dict(flat=True)

    field_error = user_update_fields_has_error(request_dict)
    if field_error:
        return jsonify({'error': field_error}), 400

    file_error = user_files_arevalid(request.files)
    if file_error:
        return jsonify({'error': file_error}), 400

    # Upload image
    imageUrl = current_user['imageUrl']
    for key, val in enumerate(request.files):
        # Only loop once, unable to perform this without loop
        filename = '{}'.format(uuid4())
        imageUrl = GoogleBucket.bucket_upload(request.files[val], filename)

    request_dict.update({'imageUrl': imageUrl})

    # Update user
    response = Database.get_collection_user().update_one({
        '_id': ObjectId(current_user['_id'])
    },
        {'$set': request_dict}
    )

    return jsonify({'payload': {'user': request_dict}})
