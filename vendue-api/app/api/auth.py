from flask import Blueprint, request, jsonify, make_response
from ..utils.bcrypt import bcrypt
from os import getenv
from datetime import datetime, timedelta
from bson import ObjectId
from bson.json_util import dumps
from json import loads
from uuid import uuid4
from time import time
import jwt

from ..utils.auth import register_fields_has_error, fields_already_taken, login_fields_has_error, review_password
from ..connections.database import Database
from ..connections.flask_mail import FlaskMail

auth = Blueprint('auth', __name__)


@auth.route('/register', methods=['POST'])
def register():
    '''
    Register user
    1. Checks for field error and duplicate data
    2. Add/ Modify appropriate data before insertion
    3. Insert the user to the database
    4. Send confirmation email and set token in database
    '''
    try:
        # Fetch input and check for field errors
        request_json = request.get_json()

        field_error = register_fields_has_error(request_json)
        del request_json['confirmPassword']
        if field_error:
            return jsonify({'error': field_error}), 400

        has_duplicate = fields_already_taken(request_json)
        if has_duplicate:
            return jsonify({'error': has_duplicate}), 403

        # Check password strength
        response = review_password(request_json['password'])
        if response.get('error', None):
            return jsonify(response), 400

        # Register the user in database
        user_password = bcrypt.generate_password_hash(
            request_json['password'])
        request_json.update({
            'confirmed': False,
            'password': user_password,
            'rating': 100,
            'location': '',
            'joinedIn': datetime.now(),
            'description': '',
            'imageUrl': 'https://storage.googleapis.com/vendue/no-img.png'
        })
        cursor = Database.get_collection_user().insert_one(request_json)

        # Send email verification and place token in database
        data = {
            'token': str(uuid4()),
            'userId': str(cursor.inserted_id),
            'purpose': 'emailVerification'
        }
        Database.get_collection_token().delete_many({'userId': data['userId']})
        Database.get_collection_token().insert_one(data)
        FlaskMail.send_confirmation_email(
            data['userId'], request_json['email'], data['token'])

        return jsonify({'payload': True})
    except Exception as e:
        print(e)
        return jsonify({'error': {'general': 'Internal Server Error'}}), 500


@auth.route('/check_password', methods=['POST'])
def check_password():
    try:
        request_json = request.get_json()
        if not request_json:
            return jsonify({'error': {'general': 'invalid request'}}), 400

        password = request_json.get('password', '')

        if not isinstance(password, str):
            return jsonify({'error': {'password': 'should be string'}}), 400
        if not password:
            return jsonify({'error': {'password': 'should not be empty'}}), 400

        response = review_password(password)
        if response.get('error', None):
            return jsonify(response), 400
        return jsonify(response)
    except Exception as e:
        print(e)
        return jsonify({'error': 'Something went wrong'}), 500


@auth.route('/login', methods=['POST'])
def login():
    '''
    Login user
    1. Checks for field error
    2. Find the user in the database, checks if exists and has correct credentials
    3. Provide Auth token as a response for request that requires credentials
    4. Set cookie
    P.S. token response format = `User id`@`jwt token`
    '''
    # Fetch input and check for field errors
    request_json = request.get_json()
    if 'remember' in request_json.keys():
        remember = request_json.pop('remember')
    field_error = login_fields_has_error(request_json)
    if field_error:
        return jsonify({'error': field_error}), 400

    # Fetch user and check for credentials
    current_user = Database.get_collection_user().find_one(
        {'username': request_json['username']})
    if not current_user:
        return make_response(jsonify({'error': {'username': 'Not found'}}),
                             401, {'WWW-Authenticate': 'Basic realm="Login required!"'})
    if not current_user.get('confirmed', True):
        return jsonify({'error': {'general': 'Account is not confirmed, please validate your email first'}}), 403
    if not bcrypt.check_password_hash(current_user['password'], request_json['password']):
        return make_response(jsonify({'error': {'password': 'Invalid'}}),
                             401, {'WWW-Authenticate': 'Basic realm="Login required!"'})

    # Token
    exp_date = datetime.now() + timedelta(days=1)
    user_id = str(current_user['_id'])

    jwt_token = jwt.encode({'_id': user_id,
                            'exp': exp_date}, getenv('SECRET_KEY'))
    token = "{}@{}".format(user_id, jwt_token.decode('UTF-8'))

    # Cookie
    resp = make_response(jsonify({'payload': {'token': token}}))
    if remember:
        ONE_MONTH = 2592000
        resp.set_cookie('Authorization', token, max_age=ONE_MONTH)
    else:
        resp.set_cookie('Authorization', token)

    return resp


@auth.route('/validate_email/<token>', methods=['GET'])
def validate_email(token):
    # Check for token and credentials
    cursor = Database.get_collection_token().find_one({'token': token})
    if not cursor:
        return jsonify({'error': {'token': 'invalid'}}), 400
    if cursor.get('purpose', None) != 'emailVerification':
        return jsonify({'error': {'token': 'invalid'}}), 400

    # Delete token and update user
    response = Database.get_collection_user().update_one({
        '_id': ObjectId(cursor['userId'])
    },  {'$set': {'confirmed': True}})

    Database.get_collection_token().delete_one({'token': token})
    return jsonify({'payload': True})


@auth.route('/reset_password/request', methods=['POST'])
def request_reset_password():
    # Fetch input and check for field errors
    request_json = request.get_json()
    if not request_json:
        return jsonify({'error': {'email': 'invalid'}})

    # Fetch user and check for credentials
    email = request_json.get('email', None)
    cursor = Database.get_collection_user().find_one(
        {'email': email}, {'username': 1, '_id': 1, 'confirmed': 1})
    if not cursor:
        return jsonify({'error': {'email': 'not registered'}}), 403
    if cursor['confirmed'] == False:
        return jsonify({'error': {'general': 'user account is not confirmed'}}), 403

    data = {
        'token': str(uuid4()),
        'userId': str(cursor['_id']),
        'purpose': 'requestCheckToken'
    }

    # Place token in database and send reset token
    Database.get_collection_token().delete_many({'userId': data['userId']})
    Database.get_collection_token().insert_one(data)
    FlaskMail.send_reset_token(
        cursor['username'], email, data['token'])

    return jsonify({'payload': True})


@auth.route('/reset_password/check/<token>', methods=['GET'])
def check_reset_password(token):
    # Fetch token and check for credentials
    cursor = Database.get_collection_token().find_one({'token': token})
    if not cursor:
        return jsonify({'error': {'token': 'invalid'}}), 400
    if cursor.get('purpose', None) != 'requestCheckToken':
        return jsonify({'error': {'token': 'invalid'}}), 400

    # Update token purpose
    data = {
        'userId': cursor['userId'],
        'token': str(uuid4()),
        'purpose': 'requestResetToken'
    }
    Database.get_collection_token().delete_one({'token': token})
    Database.get_collection_token().insert_one(data)
    return jsonify({'payload': {'token': data['token']}})


@auth.route('/reset_password/update', methods=['POST'])
def reset_password():
    # Fetch input and check for credentials
    request_json = request.get_json()
    if not request_json:
        return jsonify({'error': {'general': 'invalid request'}}), 400

    password = request_json.get('password', None)
    confirm_password = request_json.get('confirmPassword', None)
    token = request_json.get('token', None)
    valid = password and confirm_password and token
    if not valid:
        return jsonify({'error': {'general': 'invalid request'}}), 400

    # Find token in database and check for credentials
    cursor = Database.get_collection_token().find_one({'token': token})
    if not cursor:
        return jsonify({'error': {'token': 'invalid'}}), 403
    if cursor['purpose'] != 'requestResetToken':
        return jsonify({'error': {'token': 'invalid'}}), 403
    # Check password strength
    response = review_password(password)
    if response.get('error', None):
        return jsonify(response), 400
    if password != confirm_password:
        return jsonify({'confirmPassword': 'Should match password'}), 400

    # Update user
    Database.get_collection_token().delete_one({'token': token})
    Database.get_collection_user().update_one(
        {'_id': ObjectId(cursor['userId'])},
        {'$set':
         {'password': bcrypt.generate_password_hash(password)}}
    )

    return jsonify({'payload': True})
