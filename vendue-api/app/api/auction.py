from flask import Blueprint, request, jsonify, make_response
from datetime import datetime, timezone
from time import time
from bson.json_util import dumps
from bson import ObjectId, regex
from json import loads
from uuid import uuid4
from pymongo.errors import DuplicateKeyError

from ..connections.database import Database
from ..connections.google_bucket import GoogleBucket
from ..connections.message_queue import MessageQueue
from ..utils.decorator import token_required
from ..utils.auction import auction_create_fields_has_error, files_arevalid

auction = Blueprint('auction', __name__)


@auction.route('/', methods=['GET'])
def index():
    return jsonify({'payload': 'This is auction route'})


@auction.route('/create', methods=['POST'])
@token_required
def auction_create(current_user):
    '''
    Create auction
    1. Checks for field error and duplicate data
    2. Upload image to Google Bucket and receive imageUrl
    3. Add/ Modify appropriate data before insertion
    4. Insert auction to database and take its id
    5. Place its id to message queue and wait for expiration
    '''
    try:
        # Call created date early
        created_date = int(time())

        # Fetch input and check for credentials
        request_json = request.values.to_dict(flat=True)
        field_error = auction_create_fields_has_error(request_json)
        if field_error:
            return jsonify({'error': field_error}), 400

        file_error = files_arevalid(request.files)
        if file_error:
            return jsonify({'error': file_error}), 400

        # Upload image
        image_array = []
        for key, val in enumerate(request.files):
            filename = str(uuid4())
            imageUrl = GoogleBucket.bucket_upload(request.files[val], filename)
            image_array.append(imageUrl)

        # Set up for auction creation
        expired_date = int(request_json['expiredDate'])
        time_remaining = expired_date - created_date

        request_json.update({
            'seller': ObjectId(current_user['_id']),
            'imageUrls': image_array,
            'hasBuyer': False,
            'hasFinished': False,
            'createdAt': created_date,
            'expiredDate': expired_date,
            'buyer': None,
            'highestBid': request_json['initialBid'],
        })

        # Insert auction to database
        cursor = Database.get_collection_auction().insert_one(request_json)
        # Call RabbitMQ timer for completion
        response = MessageQueue.auction_timer(
            str(cursor.inserted_id), time_remaining)

        return jsonify({'payload': True})
    except Exception as e:
        print(e)
        return jsonify({'error':  'Internal Error'}), 500


@auction.route('/unfinished', methods=['GET'])
def unfinished_auctions_read():
    '''
    MonggoDB Aggregation using pipeline method
    for unwinding seller ObjectID
    '''
    pipeline = [
        {'$match': {'hasFinished': False}},
        {'$sort': {'createdAt': -1, 'name': 1}},
        {'$lookup': {
            'from': 'user',
            'localField': 'seller',
            'foreignField': '_id',
            'as': 'seller'
        }},
        {'$unwind': '$seller'},
        {'$lookup': {
            'from': 'like',
            'localField': '_id',
            'foreignField': 'auctionId',
            'as': 'likes'
        }},
        {'$lookup': {
            'from': 'user',
            'localField': 'buyer',
            'foreignField': '_id',
            'as': 'buyer'
        }},
        {'$unwind': {
            'path': '$buyer',
            'preserveNullAndEmptyArrays': True
        }},
        {'$project': {
            '_id': 1,
            'name': 1,
            'imageUrls': 1,
            'initialBid': 1,
            'highestBid': 1,
            'expiredDate': 1,
            'seller.username': 1,
            'buyer.username': '$buyer.username',
            'likes.userId': 1
        }}
    ]
    cursor = Database.get_collection_auction().aggregate(pipeline)
    payload = loads(dumps(cursor))

    return jsonify({'payload': payload})


@ auction.route('/myauctions', methods=['GET'])
@ token_required
def user_auctions(current_user):
    try:
        pipeline = [
            {'$match': {'seller': ObjectId(current_user['_id'])}},
            {'$sort': {'createdAt': -1, 'name': 1}},
            {'$lookup': {
                'from': 'user',
                'localField': 'seller',
                'foreignField': '_id',
                'as': 'seller'
            }},
            {'$unwind': '$seller'},
            {'$lookup': {
                'from': 'like',
                'localField': '_id',
                'foreignField': 'auctionId',
                'as': 'likes'
            }},
            {'$lookup': {
                'from': 'user',
                'localField': 'buyer',
                'foreignField': '_id',
                'as': 'buyer'
            }},
            {'$unwind': {
                'path': '$buyer',
                'preserveNullAndEmptyArrays': True
            }},
            {'$project': {
                '_id': 1,
                'name': 1,
                'imageUrls': 1,
                'initialBid': 1,
                'highestBid': 1,
                'expiredDate': 1,
                'seller.username': 1,
                'buyer.username': '$buyer.username',
                'likes.userId': 1,
                'hasFinished' : 1
            }}
        ]
        cursor = Database.get_collection_auction().aggregate(pipeline)
        payload = loads(dumps(cursor))

        return jsonify({'payload': payload})
    except Exception as e:
        print(e)
        return jsonify({'error': 'Internal Error'}), 500


@ auction.route('/likedauctions', methods=['GET'])
@ token_required
def liked_auctions(current_user):
    try:
        pipeline = [
            {'$match': {'userId': current_user['_id']}},
            {'$sort': {'createdAt': -1, 'name': 1}},
            {'$lookup': {
                'from': 'auction',
                'localField': 'auctionId',
                'foreignField': '_id',
                'as': 'auction'
            }},
            {'$unwind': '$auction'},
            {'$lookup': {
                'from': 'user',
                'localField': 'auction.seller',
                'foreignField': '_id',
                'as': 'seller'
            }},
            {'$lookup': {
                'from': 'user',
                'localField': 'buyer',
                'foreignField': '_id',
                'as': 'buyer'
            }},
            {'$unwind': {
                'path': '$buyer',
                'preserveNullAndEmptyArrays': True
            }},
            {'$unwind': '$seller'},
            {'$project': {
                'auction._id': 1,
                'auction.name': 1,
                'auction.imageUrls': 1,
                'auction.initialBid': 1,
                'auction.highestBid': 1,
                'auction.expiredDate': 1,
                'seller.username': 1,
                'buyer.username': '$buyer.username',
                'auction.hasFinished' : 1
            }}
        ]
        cursor = Database.get_collection_like().aggregate(pipeline)
        payload = loads(dumps(cursor))

        return jsonify({'payload': payload})
    except Exception as e:
        print(e)
        return jsonify({'error': e}), 500


@ auction.route('/name=<auction_name>', methods=['GET'])
def auctions_search(auction_name):
    '''
    MonggoDB aggregation with regex matching
    '''
    pipeline = [
        {'$match': {
            'name':
            {'$regex': regex.Regex(auction_name), '$options': 'i'}
        }},
        {'$sort': {'createdAt': -1, 'name': 1}},
        {'$lookup': {
            'from': 'user',
            'localField': 'seller',
            'foreignField': '_id',
            'as': 'seller'
        }},
        {'$unwind': '$seller'},
        {'$lookup': {
            'from': 'user',
            'localField': 'buyer',
            'foreignField': '_id',
            'as': 'buyer'
        }},
        {'$unwind': {
            'path': '$buyer',
            'preserveNullAndEmptyArrays': True
        }},
        {'$project': {
            '_id': 1,
            'name': 1,
            'imageUrls': 1,
            'initialBid': 1,
            'expiredDate': 1,
            'seller.username': 1,
            'buyer.username': '$buyer.username',
        }}
    ]
    cursor = Database.get_collection_auction().aggregate(pipeline)
    payload = loads(dumps(cursor))
    return jsonify({'payload': payload})


@ auction.route('/<auction_id>/detail', methods=['GET'])
def auction_read(auction_id):
    '''
    Fetch auction based on id
    '''
    try:
        object_id = ObjectId(auction_id)
        pipeline = [
            {'$match': {'_id': object_id}},
            {'$lookup': {
                'from': 'user',
                'localField': 'seller',
                'foreignField': '_id',
                'as': 'seller'
            }},
            {'$unwind': '$seller'},
            {'$lookup': {
                'from': 'like',
                'localField': '_id',
                'foreignField': 'auctionId',
                'as': 'likes'
            }},
            {'$lookup': {
                'from': 'user',
                'localField': 'buyer',
                'foreignField': '_id',
                'as': 'buyer'
            }},
            {'$unwind': {
                'path': '$buyer',
                'preserveNullAndEmptyArrays': True
            }},
            {'$project': {
                '_id': 1,
                'name': 1,
                'createdAt': 1,
                'imageUrls': 1,
                'initialBid': 1,
                'highestBid': 1,
                'expiredDate': 1,
                'seller.username': 1,
                'buyer.username': '$buyer.username',
                'likes.userId': 1,
                'hasFinished': 1
            }}
        ]
        cursor = Database.get_collection_auction().aggregate(pipeline)
        payload = loads(dumps(cursor))
        if not payload:
            return jsonify({'error': 'Could not find the auction'}), 404

        return jsonify({'payload': payload[0]})

    except:
        return jsonify({'error': 'Internal Error'}), 500


@ auction.route('/<auction_id>/update', methods=['PUT'])
@ token_required
def auction_update(auction_id):
    # TODO need to accomodate formData update
    auction = Database.get_collection_auction().update_one({'_id', })

    if not auction:
        return jsonify({'error': ''}), 400
    else:
        return jsonify({'payload': {
            'auction': auction
        }})


@ auction.route('/<auction_id>/comment', methods=['POST'])
@ token_required
def post_auction_comment(current_user, auction_id):
    request_json = request.get_json()

    request_json.update({
        'auctionId': auction_id,
        'userComment': ObjectId(current_user['_id']),
        'createdAt': int(time())
    })

    Database.get_collection_comment().insert_one(request_json)
    return jsonify({'payload': True})


@ auction.route('/<auction_id>/comments', methods=['GET'])
def view_auction_comments(auction_id):
    pipeline = [
        {
            '$match': {'auctionId': auction_id}
        },
        {'$sort': {'createdAt': -1}},
        {
            '$lookup': {
                'from': 'user',
                'localField': 'userComment',
                'foreignField': '_id',
                'as': 'userComment'
            }
        },
        {'$sort': {'createdAt': -1, 'name': 1}},
        {'$unwind': '$userComment'},
        {'$project': {
            '_id': 1,
            'data': 1,
            'createdAt': 1,
            'userComment._id': 1,
            'userComment.username': 1,
            'userComment.imageUrl': 1
        }}
    ]
    cursor = Database.get_collection_comment().aggregate(pipeline)
    payload = loads(dumps(cursor))

    return jsonify({'payload': {
        'comments': payload
    }})


@ auction.route('/<auction_id>/like', methods=['GET'])
@ token_required
def auction_like(current_user, auction_id):
    try:
        a_id = ObjectId(auction_id)
        cursor = Database.get_collection_auction().find({'_id': a_id})
        if not loads(dumps(cursor)):
            return jsonify({'error': 'the auction does not exists'}), 404

        data = {
            'auctionId': a_id,
            'userId': current_user['_id']
        }
        Database.get_collection_like().insert_one(data)

        return jsonify({'payload': True})
    except DuplicateKeyError:
        return jsonify({'payload': True})
    except Exception as e:
        return jsonify({'error': 'Internal Error'}), 500


@ auction.route('/<auction_id>/unlike', methods=['GET'])
@ token_required
def auction_unlike(current_user, auction_id):
    try:
        a_id = ObjectId(auction_id)
        cursor = Database.get_collection_auction().find({'_id': a_id})
        if not loads(dumps(cursor)):
            return jsonify({'error': 'the auction does not exists'}), 404

        Database.get_collection_like().delete_one(
            {'auctionId': ObjectId(auction_id),
             'userId': current_user['_id']}
        )

        return jsonify({'payload': True})
    except Exception as e:
        return jsonify({'error': 'Internal Error'}), 500


@ auction.route('/<auction_id>/bid', methods=['POST'])
@ token_required
def auction_bid(current_user, auction_id):
    try:
        cursor = Database.get_collection_auction().find_one(
            {'_id': ObjectId(auction_id)})
        auction = loads(dumps(cursor))
        current_id = ObjectId(current_user['_id'])

        if not auction:
            return jsonify({'error': 'the auction does not exists'}), 404
        if auction['seller']['$oid'] == current_user['_id']:
            return jsonify({'error': {'bid': 'you cannot bid as a seller'}}), 400
        if auction.get('buyer', None):
            if auction['buyer']['$oid'] == current_user['_id']:
                return jsonify({'error': {'bid': 'you cannot bid twice in a row'}}), 400

        request_json = request.get_json()
        if not request_json:
            return jsonify({'error': {'general': 'invalid request'}}), 400

        bid = request_json.get('bid', None)
        if not bid:
            return jsonify({'bid': 'is empty'}), 400
        bid = int(bid)
        highest_bid = auction['highestBid']
        if bid <= highest_bid:
            return jsonify({'error': {'bid': 'should be higher than current bid'}}), 400

        Database.get_collection_auction().update_one(
            {'_id': ObjectId(auction_id)},
            {'$set': {
                'buyer': ObjectId(current_user['_id']),
                'highestBid': bid
            }}
        )

        return jsonify({'payload': True})
    except Exception as e:
        print(e)
        return jsonify({'error': 'Internal Error'}), 500
