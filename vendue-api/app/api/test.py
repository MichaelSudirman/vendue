from flask import Blueprint, request, jsonify, make_response
from bson.json_util import dumps
from bson import regex, ObjectId
# import re
from json import loads
# from datetime import datetime
# from sys import exc_info
# from os import getenv
# import pika

from ..connections.database import Database
# from ..connections.message_queue import MessageQueue
# from ..connections.google_bucket import GoogleBucket
# from ..utils.auction import delete_all, files_arevalid
# from ..utils.decorator import token_required


'''
Testing routes, only used for development.
Will be deleted upon final release
'''
test = Blueprint('test', __name__)


@test.route('/', methods=['GET'])
def index():
    return jsonify({'payload': 'This is test route'})


@test.route('/auction', methods=['DELETE'])
def delete_all_auction():
    '''
    FOR DEVELOPMENT ONLY: Quick method of deleting auction
    Beware that this does not delete linked data for external instance
    (e.g Google Bucket's image, RabbitMQ's message)
    '''
    try:
        auction = Database.get_collection_auction().delete_many({})
        return 'success'
    except:
        return 'error'


@test.route('/user', methods=['DELETE'])
def delete_all_user():
    '''
    FOR DEVELOPMENT ONLY: Quick method of deleting auction
    Beware that this does not delete linked data for external instance
    (e.g Google Bucket's image, RabbitMQ's message)
    '''
    try:
        auction = Database.get_collection_user().delete_many({})
        return 'success'
    except:
        return 'error'


@test.route('/comment', methods=['DELETE'])
def delete_all_comment():
    '''
    FOR DEVELOPMENT ONLY: Quick method of deleting auction
    Beware that this does not delete linked data for external instance
    (e.g Google Bucket's image, RabbitMQ's message)
    '''
    try:
        auction = Database.get_collection_comment().delete_many({})
        return 'success'
    except:
        return 'error'


@test.route('/like', methods=['DELETE'])
def delete_all_like():
    '''
    FOR DEVELOPMENT ONLY: Quick method of deleting auction
    Beware that this does not delete linked data for external instance
    (e.g Google Bucket's image, RabbitMQ's message)
    '''
    try:
        auction = Database.get_collection_like().delete_many({})
        return 'success'
    except:
        return 'error'


@test.route('/change_buyer', methods=['GET'])
def change_buyer():
    try:
        cursor = Database.get_collection_auction().find({})
        payload = loads(dumps(cursor))
        for el in payload:
            created_at = el['createdAt']
            initial_bid = int(el['initialBid'])
            auction = Database.get_collection_auction().update_many(
                {'createdAt': created_at},
                # {'$set': {'buyer': None}},
                {'$set': {'buyer': None}}
                # {'$set': {'buyer': {'user': None, 'highestBid': initial_bid}}}
            )
        return 'success'
    except Exception as e:
        return 'error: {}'.format(e)
