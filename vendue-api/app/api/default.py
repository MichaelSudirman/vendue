from flask import Blueprint, jsonify

default = Blueprint('default', __name__)


@default.route('/', methods=['GET'])
def index():
    '''
    Vendue API does not require `/`,
    thus replace with hello world 
    to prevent default homepage error
    '''
    return jsonify({'payload': 'Hello world'})