const getUnixTime = (time) => Math.floor(new Date(time).getTime() / 1000);
const getISOString = (time) => new Date(time).toISOString();
const convertTime = (time) => {
    const date = new Date(time * 1000);
    const newDate = date.toDateString().substr(4,);
    const newTime = date.toTimeString().substr(0,5);
    return `${newDate}  ${newTime}`
}

export { getUnixTime, getISOString, convertTime };
