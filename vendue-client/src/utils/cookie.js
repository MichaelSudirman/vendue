import Cookies from 'universal-cookie';
import { isDev } from '../utils/environment'

// Auth
export const isLoggedIn = () =>
    getAuthCookie() ? true : false;

export const getUserId = () => {
    const authCookie = getAuthCookie();
    return authCookie && authCookie.split('@')[0];
}

export const getToken = () => {
    const authCookie = getAuthCookie();
    return authCookie && authCookie.split('@')[1];
}

export const setAuth = (token, willRemember) =>
    isDev() && (
        !willRemember ?
            cookies.set('Authorization', token) :
            cookies.set('Authorization', token, { maxAge: REMEMBERED_USER })
    )

export const removeAuth = () =>
    cookies.remove('Authorization')

const getAuthCookie = () =>
    cookies.get('Authorization')

// 1 month of expiry age
const REMEMBERED_USER = 2592000;
// Cookie object
const cookies = new Cookies();