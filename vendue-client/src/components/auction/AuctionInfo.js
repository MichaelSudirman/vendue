import React, { Component, Fragment } from "react";
// Components and utils
import countdown from "../../utils/countdown";
import LikeButton from './LikeButton';
import BidButton from './BidButton';
import MyCarousel from '../common/MyCarousel';
import { convertTime } from '../../utils/common';
// Actions
// Material UI core imports
import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";


const styles = (theme) => ({
    ...theme.global,
    pageTitle: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    imageContainer: {
        display: 'flex',
        flexDirection: 'row'
    }
});

class AuctionInfo extends Component {
    parentCallBack = () =>
        this.props.parentCallBack();

    render() {
        console.log(this.props)
        const { classes,
            auction: {
                _id: { $oid: auctionId },
                name,
                imageUrls,
                createdAt,
                condition,
                expiredDate,
                initialBid,
                highestBid,
                seller: { username: sellerName },
                buyer: { username: buyerName },
                hasFinished,
                likes
            } } = this.props
        return (
            <Fragment>
                <Card className={classes.root}>
                    <CardContent >
                        <div className={classes.pageTitle}>
                            <Typography variant="h5" className={`${classes.pageTitle}`}>
                                {name}
                            </Typography>
                            <div style={{ display: "flex", direction: 'row', justifyContent: "center" }}>
                                <MyCarousel images={imageUrls} />
                            </div>

                        </div>
                        <Grid container spacing={0}>
                            <Grid item sm={3} xs={12} />
                            <Grid item sm={6} xs={12}>
                                <Table className={classes.table} aria-label="simple table">
                                    <TableBody>
                                        <TableRow key='TableRow InitialBid'>
                                            <TableCell component="th" scope="row"><Typography>Initial Bid</Typography></TableCell>
                                            <TableCell align="right"><Typography>${initialBid} by {sellerName}</Typography></TableCell>
                                        </TableRow>
                                        <TableRow key='TableRow HighestBid'>
                                            <TableCell component="th" scope="row"><Typography>Highest Bid</Typography></TableCell>
                                            <TableCell align="right"><Typography>{buyerName ?
                                                (<Fragment>{highestBid} by {buyerName}</Fragment>) :
                                                (<Fragment>No bidding yet</Fragment>)}</Typography></TableCell>
                                        </TableRow>
                                        <TableRow key='TableRow CreatedOn'>
                                            <TableCell component="th" scope="row"><Typography>Created on</Typography></TableCell>
                                            <TableCell align="right"><Typography>{convertTime(createdAt)}</Typography></TableCell>
                                        </TableRow>
                                        <TableRow key='TableRow ExpiredOn'>
                                            <TableCell component="th" scope="row"><Typography>Expired on</Typography></TableCell>
                                            <TableCell align="right"><Typography>{convertTime(expiredDate)}</Typography></TableCell>
                                        </TableRow>
                                        <TableRow key='TableRow Status'>
                                            <TableCell component="th" scope="row"><Typography>Status</Typography></TableCell>
                                            <TableCell align="right"><Typography>{hasFinished ? 'has finished' : 'on-going'}</Typography></TableCell>
                                        </TableRow>
                                    </TableBody>
                                </Table>
                                <br />
                                <div style={{ display: 'flex', justifyContent: 'center' }}>
                                    <LikeButton auctionId={auctionId} userIdList={likes} />
                                    {hasFinished ?
                                        <BidButton disabled />
                                        :
                                        <BidButton auctionId={auctionId} highestBid={highestBid}
                                            buyerName={buyerName} parentCallBack={this.parentCallBack} />
                                    }
                                </div>
                            </Grid>
                            <Grid item sm={3} xs={12}></Grid>
                        </Grid>
                    </CardContent>
                </Card>
            </Fragment >
        )
    }
}

export default withStyles(styles)(AuctionInfo);
