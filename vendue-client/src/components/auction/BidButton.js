import React, { Component, Fragment } from "react";

// actions
import { bidAuction } from "../../actions/dataActions";
// Material UI Core Imports
import withStyles from "@material-ui/core/styles/withStyles";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import CircularProgressIcon from "@material-ui/core/CircularProgress";
import Typography from "@material-ui/core/Typography";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
// Material UI Icon Imports
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';

const styles = (theme) => ({
    ...theme.global,
});

class BidButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            loading: false,
            bid: 0,
            errors: {},
        };
    }

    handleOpen = () => this.setState({ open: true, bid: this.props.highestBid });
    handleClose = () => this.setState({ open: false });
    handleChange = e => this.setState({ [e.target.name]: e.target.value });
    handleSubmit = e => {
        this.setState({ loading: true })
        e.preventDefault();
        bidAuction({ 'bid': this.state.bid }, this.props.auctionId)
            .then(res => {
                this.setState({ open: false, loading: false });
                this.props.parentCallBack();
            })
            .catch(err => this.setState({ errors: err.error, loading: false }));
    }
    componentDidMount() {
        this.setState({ bid: this.props.highestBid });
    }

    render() {
        const { classes, auctionId, buyerName, disabled } = this.props;
        const { bid, open, errors, loading } = this.state;
        return (
            <Fragment>
                <Button
                    variant="contained"
                    color="primary"
                    onClick={this.handleOpen}
                    disabled={disabled}
                >
                    <ShoppingCartIcon />
                    Place your Bid
                </Button>
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">Place your Bid</DialogTitle>
                    <DialogContent>
                        <TextField
                            id="bid"
                            name="bid"
                            label="Place your bid"
                            type="number"
                            value={bid}
                            placeholder="Your bid in Australian Dollar (AUD)"
                            error={errors.bid ? true : false}
                            helperText={errors.bid}
                            className={classes.textField}
                            onChange={this.handleChange}
                            fullWidth
                        />

                        {errors.general && (
                            <Typography variant="body2" className={classes.customError}>
                                {errors.general}
                            </Typography>
                        )}
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="secondary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleSubmit} color="primary" autoFocus>
                            Bid
                            {loading && (
                                <CircularProgressIcon
                                    size={30}
                                    className={classes.progress}
                                />
                            )}
                        </Button>
                    </DialogActions>
                </Dialog>
            </Fragment>
        )
    }

}

export default withStyles(styles)(BidButton);
