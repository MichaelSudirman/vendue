import React, { Component, Fragment } from 'react';
import PlacesAutocomplete from 'react-places-autocomplete';
// Material UI Core Imports
import TextField from "@material-ui/core/TextField";

class LocationSearchInput extends Component {
    constructor(props) {
        super(props);
        this.state = { location: '' };
    }

    handleChange = location => {
        this.setState({ location });
        this.props.parentCallback(location);
    }

    handleSelect = location => {
        this.setState({ location });
        this.props.parentCallback(location);
    };

    componentDidMount() {
        this.setState({ location: this.props.location })
    }

    render() {
        const { error, classes } = this.props;
        const { location } = this.state;

        return (
            <PlacesAutocomplete
                value={this.state.location}
                onChange={this.handleChange}
                onSelect={this.handleSelect}
            >
                {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                    <div className={classes.textField}>
                        <TextField
                            id="location"
                            name="location"
                            label="Location"
                            placeholder="Tell more about your current location"
                            value={location}
                            error={error ? true : false}
                            helperText={error}
                            className={classes.textField}
                            onChange={this.handleChange}
                            fullWidth
                            {...getInputProps({
                                placeholder: 'Search Places ...',
                                className: 'location-search-input',
                            })}
                        />
                        <div className={classes.autoCompleteDropDown}>
                            {loading && <div>Loading...</div>}
                            {suggestions.map(suggestion => {
                                const className = suggestion.active
                                    ? 'suggestion-item--active'
                                    : 'suggestion-item';
                                // inline style for demonstration purpose
                                const style = suggestion.active
                                    ? { color: '#3f51b5', cursor: 'pointer' }
                                    : { cursor: 'pointer' };
                                return (
                                    <div
                                        {...getSuggestionItemProps(suggestion, {
                                            className,
                                            style,
                                        })}
                                    >
                                        <span
                                            className={classes.autoCompleteSuggestion}
                                        >{suggestion.description}</span>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                )
                }
            </PlacesAutocomplete>
        );
    }
}

export default LocationSearchInput