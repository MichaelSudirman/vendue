import React from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import MobileStepper from "@material-ui/core/MobileStepper";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import SwipeableViews from "react-swipeable-views";
import { autoPlay } from "react-swipeable-views-utils";

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const useStyles = makeStyles(theme => ({
  root: {
    maxWidth: 320,
    flexGrow: 1,
    display: "flex",
    flexDirection: "column",
    position: "relative"
  },
  header: {
    color: "white",
    display: "flex",
    alignItems: "center",
    height: 50,
    width: "100%",
    paddingLeft: theme.spacing(4),
    backgroundColor: "rgba(0, 0, 0, 0.1)",
    position: "absolute",
    top: 0,
    left: 0
  },
  img: {
    display: "block",
    maxWidth: 800,
    overflow: "hidden",
    width: "100%"
  },
  stepper: {
    position: "relative",
    top: -32,
    backgroundColor: "transparent"
  },
  buttonsContainer: {
    position: "absolute",
    height: "100%",
    width: "100%"
  },
  buttons: {
    display: "flex",
    justifyContent: "space-between",
    height: "100%",
    alignItems: "center"
  },
  button: {
    backgroundColor: "rgba(0, 0, 0, 0.1)",
    color: "white",
    margin: "0 8px"
  }
}));

function MyCarousel({ images }) {
  const classes = useStyles();
  const theme = useTheme();
  const [activeStep, setActiveStep] = React.useState(0);
  const maxSteps = images.length;

  function handleNext() {
    const newStep = activeStep === maxSteps - 1 ? 0 : activeStep + 1;
    setActiveStep(newStep);
  }

  function handleBack() {
    const newStep = activeStep === 0 ? maxSteps - 1 : activeStep - 1;
    setActiveStep(newStep);
  }

  function handleStepChange(step) {
    setActiveStep(step);
  }

  return (
    <div className={classes.root}>
      <AutoPlaySwipeableViews
        axis={theme.direction === "rtl" ? "x-reverse" : "x"}
        index={activeStep}
        onChangeIndex={handleStepChange}
        enableMouseEvents
      >
        {images.map((img, index) => (
          <div key={`thumbnail ${index}`} style={{height:320,width:320}}>
            {Math.abs(activeStep - index) <= 2 ? (
              <img
                className={classes.img}
                src={img}
                alt={`auction thumbnail ${index}`}
              />
            ) : null}
          </div>
        ))}
      </AutoPlaySwipeableViews>
      <div className={classes.buttonsContainer}>
        <div className={classes.buttons}>
          <IconButton
            onClick={handleBack}
            className={classes.button}
          >
            {theme.direction === "rtl" ? (
              <KeyboardArrowRight />
            ) : (
                <KeyboardArrowLeft />
              )}
          </IconButton>
          <IconButton
            onClick={handleNext}
            className={classes.button}
          >
            {theme.direction === "rtl" ? (
              <KeyboardArrowLeft />
            ) : (
                <KeyboardArrowRight />
              )}
          </IconButton>
        </div>
      </div>
      <MobileStepper
        steps={maxSteps}
        position="static"
        variant="dots"
        activeStep={activeStep}
        className={classes.stepper}
        backButton={<div />}
        nextButton={<div />}
      />
    </div>
  );
}

export default MyCarousel;
