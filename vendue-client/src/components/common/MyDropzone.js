import React, { Fragment } from "react";
import Dropzone from "react-dropzone";
import imageCompression from 'browser-image-compression';

const styles = {
  baseStyle: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: "20px",
    borderWidth: 2,
    borderRadius: 2,
    borderColor: "#eeeeee",
    borderStyle: "dashed",
    backgroundColor: "#fafafa",
    color: "#bdbdbd",
    outline: "none",
    transition: "border .24s ease-in-out",
  },
};

function MyDropzone(props) {
  const { compress, parentCallback } = props;
  const style = styles.baseStyle;

  const callParent = (acceptedFiles) => parentCallback(acceptedFiles);
  const compressUpload = (acceptedFiles) => {
    if (acceptedFiles.length === 0)
      return;
    const imageFile = acceptedFiles[0];

    var options = {
      maxSizeMB: 1,
      maxWidthOrHeight: 128,
      useWebWorker: true
    }
    imageCompression(imageFile, options)
      .then(compressedFile => {
        const fileData = new File([compressedFile], imageFile.name,
          { 'type': imageFile.type });
        callParent([fileData]);
      })
      .catch(err => console.log(err));
  }

  return (
    <Fragment>
      <Dropzone
        accept="image/*"
        maxSize={10000000}
        onDrop={(acceptedFiles) => {
          compress ?
            compressUpload(acceptedFiles) :
            callParent(acceptedFiles)
        }}
      >
        {({ getRootProps, getInputProps }) => (
          <div {...getRootProps({ style })}>
            <input {...getInputProps()} />
            <p>Drag 'n' drop, or click to select image(s)</p>
          </div>
        )}
      </Dropzone>
    </Fragment>
  );
}

export default MyDropzone;
