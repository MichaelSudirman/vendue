import axios from "axios";
import { setAuth, removeAuth } from "../utils/cookie";

export const signupUser = data => {
  const newUserData = {
    username: data.username,
    email: data.email,
    password: data.password,
    confirmPassword: data.confirmPassword,
  };

  return axios
    .post("/register", newUserData)
    .then(res => res.data)
    .catch(err => {
      throw err.response.data;
    });
};

export const loginUser = data => {
  const userData = {
    username: data.username,
    password: data.password,
    remember: data.remember
  };

  return axios
    .post("/login", userData)
    .then(res => {
      const token = res.data.payload.token;
      setAuth(token, data.remember)
      window.location.href = "/";
      return res.data;
    })
    .catch(err => {
      throw err.response.data;
    });
};

export const checkPasswordStrength = data => {
  return axios
    .post('/check_password',data)
    .then(res => res.data)
    .catch(err => {
      throw err.response.data;
    })
}

export const logoutUser = () => {
  removeAuth();
  delete axios.defaults.headers.common["Authorization"];
  window.location.href = "/login";
};

export const validateUserEmail = token => {
  return axios
    .get(`/validate_email/${token}`)
    .then(res => res.data)
    .catch(err => {
      throw err.response.data
    })
}

export const requestResetPasswordToken = data => {
  return axios
    .post('/reset_password/request', data)
    .then(res => res.data)
    .catch(err => {
      throw err.response.data
    })
}

export const checkResetPasswordToken = (token) => {
  return axios
    .get(`/reset_password/check/${token}`)
    .then(res => res.data.payload)
    .catch(err => {
      throw err.response.data
    })
}

export const updateUserPassword = data => {
  return axios
    .post('/reset_password/update', data)
    .then(res => res.data.payload)
    .catch(err => {
      throw err.response.data
    })
}

export const readMyProfile = () => {
  return axios
    .get("/user/me")
    .then(res => res.data.payload)
    .catch(err => {
      throw err.response.data;
    });
};

export function updateProfile(data) {
  const formData = new FormData();
  formData.append('description', data.description);
  formData.append('location', data.location);
  let counter = 0;
  data.files.forEach((file) => {
    formData.append(`image ${counter}`, file)
  });
  return axios
    .put("user/update", formData)
    .then(res => res.data.payload)
    .catch(err => {
      throw err.response.data;
    });
};
