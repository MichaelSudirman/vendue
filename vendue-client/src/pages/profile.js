import React, { Component, Fragment } from "react";
// Components and utils
import UserDialog from "../components/user/UserDialog";
import { convertTime } from '../utils/common';
// Actions
import { readMyProfile } from "../actions/userActions";
// Material UI core imports
import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Grid from "@material-ui/core/Grid";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";

const styles = (theme) => ({
  ...theme.global,
  profileTitle: {
    height: 20
  },
  notFullWidth: {
    display: 'inlineBlock'
  },
  imageBox: {
    borderRadius: 1,
    border: "1px solid rgb(245, 245, 245)",
    boxShadow:
      "0px 2px 1px -1px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 1px 3px 0px rgba(0,0,0,0.12);",

  },
});

class profile extends Component {
  constructor(props) {
    super(props)
    this.handler = this.handler.bind(this)
    this.state = {
      user: null,
      loading: true,
    };
  }
  handler = () => {
    this.setState({ loading: true })
    this.fetchProfile()
  }
  componentDidMount = () => this.fetchProfile()
  fetchProfile = () => {
    readMyProfile()
      .then((res) => this.setState({ loading: false, user: res.user }))
      .catch((err) => console.log(err));
  }
  render() {
    const classes = this.props;
    const { user, loading } = this.state;
    return (
      <Fragment>
        {loading ? (
          "loading..."
        ) : (
            <Fragment>
              <Grid container className={classes.form} spacing={0}>
                <Grid item sm={3} />
                <Grid item sm={6} xs={12}>
                  <Card className={classes.root}>
                    <CardContent>
                      <Grid container className={classes.pageTitle} >
                        <Grid item sm={1} xs={1}>
                        </Grid>
                        <Grid item sm={10} xs={10}>
                          <Typography variant="h5" className={`${classes.pageTitle}`} align="center" >
                            My Profile
                          </Typography>

                        </Grid>
                        <Grid item sm={1} xs={1}>
                          <UserDialog user={user} handler={this.handler} />
                        </Grid>
                      </Grid>
                      <div className={classes.imageBox} style={{ display: "flex", justifyContent: "center" }}>
                        <img
                          src={user.imageUrl}
                          height={64}
                          width={64}
                          alt="profile"
                        />
                      </div>
                      <Table className={classes.table} aria-label="simple table">
                        <TableBody>
                          <TableRow key='TableRow Username'>
                            <TableCell component="th" scope="row"><Typography>Username</Typography></TableCell>
                            <TableCell align="right"><Typography>{user.username}</Typography></TableCell>
                          </TableRow>
                          <TableRow key='TableRow Email'>
                            <TableCell component="th" scope="row"><Typography>Email</Typography></TableCell>
                            <TableCell align="right"><Typography>{user.email}</Typography></TableCell>
                          </TableRow>
                          <TableRow key='TableRow Description'>
                            <TableCell component="th" scope="row"><Typography>Description</Typography></TableCell>
                            <TableCell align="right"><Typography>{user.description ? user.description : "Not available"}</Typography></TableCell>
                          </TableRow>
                          <TableRow key='TableRow Joined from'>
                            <TableCell component="th" scope="row"><Typography>Joined from</Typography></TableCell>
                            <TableCell align="right"><Typography>{convertTime(user.joinedIn.$date)}</Typography></TableCell>
                          </TableRow>
                          <TableRow key='TableRow Location'>
                            <TableCell component="th" scope="row"><Typography>Location</Typography></TableCell>
                            <TableCell align="right"><Typography>
                              {user.location ? user.location : "Not available"}</Typography></TableCell>
                          </TableRow>
                          <TableRow key='TableRow Rating'>
                            <TableCell component="th" scope="row"><Typography>Rating</Typography></TableCell>
                            <TableCell align="right"><Typography>{user.rating}</Typography></TableCell>
                          </TableRow>
                        </TableBody>
                      </Table>
                    </CardContent>
                  </Card>
                </Grid>
                <Grid item sm={3} />
               </Grid>
            </Fragment >
          )
        }
      </Fragment>
    );
  }
}

export default withStyles(styles)(profile);
